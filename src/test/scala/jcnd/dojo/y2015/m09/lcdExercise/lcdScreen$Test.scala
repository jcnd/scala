package jcnd.dojo.y2015.m09.lcdExercise

import org.scalatest.FunSuite

class lcdScreen$Test extends FunSuite {

  val expected = (" _   _       _  ", " _| | |   |  _| ","|_  |_|   | |_  ")

  test("testGetLCDNaive") {
      assert(expected == lcdScreen.getLCDNaive(2012))
  }

  test("testGetLCDMapping") {
    assert(expected == lcdScreen.getLCDMapping(2012))
  }

  test("testGetLCDRecursive") {
    val expected = ("  _   _       _ ","  _| | |   |  _|"," |_  |_|   | |_ ")
    assert(expected== lcdScreen.getLCDRecursive(2012))
  }

  test("testGetLCDTailRecursive") {
    assert(expected == lcdScreen.getLCDTailRecursive(2012))
  }


}
