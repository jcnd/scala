package jcnd.dojo.y2015.m09.lcdExercise

import org.scalameter.Bench
import org.scalameter.api._

object LcdScreenBenchmark
  extends Bench.LocalTime {

  val sizes = Gen.range("size")(3000, 15000, 3000)

  val ranges = for {
    size <- sizes
  } yield 0 until size

  performance of "LcdScreen" in {
    measure method "getLCDNaive" in {
      using(ranges) in {
        r => r.map(lcdScreen.getLCDNaive(_))
      }
    }

    measure method "getLCDMapping" in {
      using(ranges) in {
        r => r.map(lcdScreen.getLCDMapping(_))
      }
    }

    measure method "getLCDRecursive" in {
      using(ranges) in {
        r => r.map(lcdScreen.getLCDRecursive(_))
      }
    }

    measure method "getLCDTailRecursive" in {
      using(ranges) in {
        r => r.map(lcdScreen.getLCDTailRecursive(_))
      }
    }


  }
 }
