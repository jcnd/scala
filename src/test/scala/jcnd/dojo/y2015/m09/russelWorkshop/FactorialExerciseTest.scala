package jcnd.dojo.y2015.m09.russelWorkshop

import jcnd.dojo.y2015.m09.russelWorkshop.exercise.CompoundInterestExercise
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, FunSuite}

/**
 * Created by jcharlet on 02/09/15.
 */
class FactorialExerciseTest extends FunSuite with Matchers with TableDrivenPropertyChecks {
  val functions = Table(
    ("algorithm", "name"),
    (CompoundInterestExercise.calculateCompoundInterestOnYear _, "calculateCompoundInterestOnYear")
  )

  val inputData = Table(
    ("principal", "yearNumber", "interest", "expectedOutput"),
    (100,1,0.03,103),
    (100,1,0.05,105),
    (100,1,0.07,107),
    (100,2,0.03,106.09),
    (100,2,0.05,110.25),
    (100,2,0.07,114.49),
//    (100,3,0.03,103),
//    (100,3,0.05,105),
//    (100,3,0.07,107),
//    (100,4,0.03,103),
//    (100,4,0.05,105),
//    (100,4,0.07,107),
//    (100,5,0.03,103),
//    (100,5,0.05,105),
//    (100,5,0.07,107),
//    (100,6,0.03,103),
//    (100,6,0.05,105),
//    (100,6,0.07,107),
//    (100,7,0.03,103),
//    (100,7,0.05,105),
//    (100,7,0.07,107),
//    (100,8,0.03,103),
//    (100,8,0.05,105),
//    (100,8,0.07,107),
//    (100,9,0.03,103),
//    (100,9,0.05,105),
//    (100,9,0.07,107),
    (100,10,0.03,134.39),
    (100,10,0.05,162.89),
    (100,10,0.07,196.72)
  )


  //FIXME JCT for some reason does not compile:
//  forAll (inputData) {(n:Int, f:BigInt) =>
//    forAll (functions) {(algorithm:Function[Int,Int,Float], name:String) =>
//      test(name + " " + n) { algorithm(n) should equal (f) }
//    }
//  }

}
