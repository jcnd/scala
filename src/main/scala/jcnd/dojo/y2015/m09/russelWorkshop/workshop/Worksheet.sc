for (i <- 0 until 10){
  println(i)
}
//explicit iteration
for (i <- 0 to 10){
  println(i)
}

val characters = Vector('a','a','b','a','b','c')
characters.indices

var nbOfA=0
for (character <- characters){
  if(character == 'a')
    nbOfA+=1;
}
println("nb of a " + nbOfA)

//implicit iteration
characters.count(c => c == 'a')
characters.count(_ == 'a')

def countem(s:Seq[Char],c:Char):Int = s.count(_ == c)
def countem2(s:Seq[Char],c:Char):Int = s.count(_ == c)
val countem3 = (s:Seq[Char],c:Char) => s.count(_ == c)
println("nb of b: " + countem(characters, 'b').toString)
println("nb of b: " + countem2(characters, 'b').toString)
println("nb of b: " + countem3(characters, 'b').toString)

val count = (for {c <- characters;if c == 'a'} yield c).length
println("nb of a: " + count)

characters.foreach(print);println

println("printing with mapreduce")
//println(Map(characters).Reduce(print))

(1 to 10)
