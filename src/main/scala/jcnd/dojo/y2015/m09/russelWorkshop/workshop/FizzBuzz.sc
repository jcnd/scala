//Write a program that prints the numbers from 1 to 100,
//but for multiples of three print "Fizz" instead of the number
//for multiples of five print "Buzz" instead of the number
//for multiples of both three and five print "FizzBuzz" instead

//for(i <- 1 to 15){
//  if(i%3== 0 && i%5==0){
//    println("FizzBuzz")
//  }else if(i%3== 0){
//    println("Fizz")
//  }else if(i%5 == 0){
//    println("Buzz")
//  }else{
//    println(i)
//  }
//}

//(1 to 15).foreach((i) => (
//  if(i%3== 0 && i%5==0)
//    println("FizzBuzz")
//  else if(i%3==0)
//    println("Fizz")
//  else if(i%5==0)
//    println("Buzz")
//  else
//    println(i)
//  ))

//def printFizzBuzzOnCollection(collection:Range) = {
//  collection.foreach((i) =>
//    if(i%3== 0 && i%5==0)
//      println("FizzBuzz")
//    else if(i%3==0)
//      println("Fizz")
//    else if(i%5==0)
//      println("Buzz")
//    else
//      println(i))
//}

//def printFizzBuzzOnCollection(collection: Range) = {
//  collection.foreach((i) =>
//    println(
//      if (i % 3 == 0 && i % 5 == 0) "FizzBuzz"
//      else if (i % 3 == 0) "Fizz"
//      else if (i % 5 == 0) "Buzz"
//      else i
//    )
//  )
//}

//def printFizzBuzzOnCollection(collection: Range) = {
//  collection.foreach((i) =>
//    println(
//      (i % 3, i % 5) match{
//        case (0,0) => "FizzBuzz"
//        case (0,_) => "Fizz"
//        case (_,0) => "Buzz"
//        case (_,_) => i
//      }
//    )
//  )
//}

def printFizzBuzzOnCollection(collection: Range) = {
    collection map ((i) =>
        (i % 3, i % 5) match{
          case (0,0) => "FizzBuzz"
          case (0,_) => "Fizz"
          case (_,0) => "Buzz"
          case (_,_) => i
        }
    ) foreach println
  }

printFizzBuzzOnCollection((1 to 15))


