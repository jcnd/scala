package jcnd.dojo.y2015.m09.russelWorkshop.exercise

/**
 * Created by jcharlet on 02/09/15.
 */
object CompoundInterestExercise {
/*
Write a program to calculate and print compound interest
for a given principal
for a given term
for some given interest rates

For principal of £100, a period of 10 years at 3%, 5% and 7% interest, the output should resemble
103.00 105.00 107.00
106.09 110.25 114.49
...
134.39 162.89 196.72
 */

  def main(args: Array[String]) {
    println(calculateCompoundInterestOnYear(100, 10,0.03f));
  }

  def validate(year: Int) = assert(year>0)

  def calculateCompoundInterestOnYear(principal:Int, year:Int, interest:Float):Float = {
    validate(year)
    if (year==0) principal
    else calculateCompoundInterestOnYear(principal, year - 1, interest) * (1+interest)
  }
}
