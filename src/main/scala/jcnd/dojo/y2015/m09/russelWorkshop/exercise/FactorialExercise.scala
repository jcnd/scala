package jcnd.dojo.y2015.m09.russelWorkshop.exercise

/**
 * Created by jcharlet on 02/09/15.
 */
object FactorialExercise {
  /*
  Create and write tests for
  a few implementations of many functions to calculate the factorial of the argument.

  F(0)=1
  F(n)=n*F(n-1) for every n>0

   */

  def main(args: Array[String]) {
    println("Factorial of " +1 + " " + getFactorial(1))
    println("Factorial of " +2 + " " + getFactorial(2))
  }

  def getFactorial(n:Int): Unit ={
    if(n==0) 1
    else getFactorial(n-1)
  }

}
