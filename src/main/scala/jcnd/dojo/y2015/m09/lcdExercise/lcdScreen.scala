package jcnd.dojo.y2015.m09.lcdExercise

import scala.annotation.tailrec

object lcdScreen {

  //  ._.   ...   ._.   ._.   ...   ._.   ._.   ._.   ._.   ._.
  //  |.|   ..|   ._|   ._|   |_|   |_.   |_.   ..|   |_|   |_|
  //  |_|   ..|   |_.   ._|   ..|   ._|   |_|   ..|   |_|   ..|

  var zero = Vector(" _ ", "| |", "|_|")
  var one = Vector("   ", "  |", "  |")
  var two = Vector(" _ ", " _|", "|_ ")

  var numberMapping = Map(
    0 -> zero, 1 -> one, 2 -> two,
    3 -> zero, 4 -> one, 5 -> two,
    6 -> zero, 7 -> one, 8 -> two,
    9 -> zero
  )


  def main(args: Array[String]) {
    val res = getLCDRecursive(2012)

    println(res._1)
    println(res._2)
    println(res._3)
  }


  def getLCDMapping(number: Int): (String, String, String) = {
    //Return type not mandatory
    //     number.toString.toCharArray map (c => {
    //       val digit: Vector[String] = numberMapping(Integer.parseInt(c.toString))  INFIX form
    //       (digit(0) + " ", digit(1) + " ", digit(2) + " ")
    //     }) reduce ((A, B) => (A._1 + B._1, A._2 + B._2, A._3 + B._3))
    number.toString.toCharArray.map(c => {
      val digit: Vector[String] = numberMapping(Integer.parseInt(c.toString))
      (digit(0) + " ", digit(1) + " ", digit(2) + " ")
    }).reduce((A, B) => (A._1 + B._1, A._2 + B._2, A._3 + B._3))
  }

  def getLCDNaive(number: Int) = {
    var firstLine = ""
    var secondLine = ""
    var thirdLine = ""

    number.toString.toCharArray.foreach {
      c => {
        val digit
        //         : Option[Vector[String]]
        = getStringForOneDigit(Integer.parseInt(c.toString))

        digit match {
          case Some(y) => {
            firstLine += y(0) + " "
            secondLine += y(1) + " "
            thirdLine += y(2) + " "
          }
          case None => println("Boom")
        }
      }
    }

    (firstLine, secondLine, thirdLine)
    //    getStringForOneDigit(number).foreach(x => x.toVector.foreach(x => print(x + " SEPARATOR ")))
  }

  def getStringForOneDigit(digit: Int): Option[Vector[String]] = {
    numberMapping.get(digit)
  }


  def getLCDRecursive(number: Int): (String, String, String) =
    number match {
      case 0 => ("", "", "")
      case _: Int => {
        val x = number / 10
        val mod = number % 10
        val head = numberMapping(mod)
        val rec = getLCDRecursive(x)

        ( rec._1+ " " + head(0),  rec._2+ " " + head(1), rec._3+ " " +  head(2))
      }
    }


  def getLCDTailRecursive(number: Int) = {

    @tailrec def getLCDTailRecursiveAcc(acc: (String, String, String), number: Int): (String, String, String) =
       {
        if(number == 0)  acc
        else  {
          val x = number / 10
          val mod = number % 10
          val head = numberMapping(mod)
           getLCDTailRecursiveAcc((head(0)+ " " + acc._1 , head(1) + " " + acc._2 ,head(2) + " " + acc._3), x)
        }
      }
    getLCDTailRecursiveAcc(("", "", ""), number)
  }
}
